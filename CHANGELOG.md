# Changelog
All notable changes to this repository will be documented in this file.

## [0.0.3] - 2019-06-01
### Changed
- Update _package.json_ and remove suport of Monero (XMR) :/

## [0.0.3] - 2019-06-01
### Added
- A new independient style sheet, set a default colors and fonts.

## [0.0.2] - 2019-05-22
### Changed
- Edit some information in _README.md_ _CHANGELOG.md_ _.gitignore_ and others.

### Added
- Basic configuration, dependences and scripts of _package.json_, and _webpack.config.js_
- Bootstrap base template.

## [0.0.1] - 2019-05-22
### Added
- Basic files of any repository as _README.md_ _CHANGELOG.md_ _.gitignore_ and others.
- _lICENSE_ file with the legal information concerning this repository.
