<img width="200" src="https://gitlab.com/kAoi97/brand/raw/master/Imagotype/Bitmap/kAoi97%20horisontal%20Imagotype.png" /><hr>

# WebMiner

* Visit my online implementation of this repository [here](http://webminer.kaoi97.net) and see what it is about.

Below you will find useful information when you want to implement your own version of this web miner on your site. Remember that this is a simple [Webchain](https://webchain.network/) miner built to be mined via websites. Based on the script provided by CoinIMP for the Webchain websites mining.

## Table of Contents

1. [Freatures](#freatures)
2. [Clone the Repository](#clone-the-repository)
3. [Contributing](#contributing)
4. [Changelog](#changelog)
5. [Support](#support)
6. [License](#license)

## Freatures

This web miner makes an articulated use of several technologies such as [Boostrap](https://getbootstrap.com/) and [Webpack](https://webpack.js.org/), in addition to the script provided by [CoinIMP](https://www.coinimp.com/). To have a friendly interface and easy interaction with the end user.

* Use the resources of Internet of Things or any device that can open a browser and render JavaScript to mine.
* 100% Free under [Cretive Commons License](#license).
* Fully Responsive Interface.
* Easily Customizable.

## Clone the Repository

You can clone my repository and be up to date with the latest changes and additions to the repository later;simply have installed [git](https://git-scm.com/) on your computer, open a terminal in the folder where you want to store the repository and execute the following command.

```
git clone https://gitlab.com/kAoi97/webminer.git

```
### Usage
First of all you have to have installed the application Node.js on your device, for this visit [this link](https://nodejs.org/en/download/) and download the version according to your operating system.

**1. Edit a few settings in _webpack.config.json_ file**

First of all, you can edit aspects such as the URL and the port where you want to run your web server ( Yes!!, the project includes a web server itself, the default URL is _http://localhost:1197_ ), or the operations that certain plugins will execute when compiling the code, feel free to check the file and configure it to your liking.

**2. Install all dependencies**

To install all the dependencies, both the indespensables as Bootstrap to compile the project, as others of the development environment as Uglify to obfuscate the code to export are installed automatically with the command `npm install` that downloads and prepares the packages contained in the list of the _package.json_ file  (execute the command in the same location of this file).

<img src="screenshots/npm-install.gif">

**3. Compile the code**

When executing `npm run build` (shortcut defined in the _package.json_ file) webpack is being instructed to run and thus start the previously configured sentences in the _webpack.config.js_ file, in which it takes all the contents of the _/src_ directory and generates as exit a new _/dist_ directory with the project in production version.

<img src="screenshots/npm-run-build.gif">


**4. Run a local web server**

Once webpack is executed and with all the code ready, it is time to run the web server with the `npm run server` command ( also defined in _package.json_ ) that will enable the URL configured in the first step, as well as the function of listening for any change in the _/src_ directory to compile it again and reload the browser automatically.

<img src="screenshots/npm-run-server.gif">

## Contributing

### Forks and Merge Requests

## Changelog

If you have cloned this repository before, do not forget to check the [changelog](CHANGELOG.md) to make sure you are working with the _latest changes uploaded_ to the repository.

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me through the different means and networks available below, I will be attentive to answer any questions in the shortest possible time:

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  


## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">kAoi97 WebMiner</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/leokaoi97" property="cc:attributionName" rel="cc:attributionURL">Leonardo Alvarado Guio</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/terms-and-conditions" rel="cc:morePermissions">https://www.kaoi97.net/terms-and-conditions</a>.
